#pragma once
#include <fc/shared_ptr.hpp>
#include <fc/unique_ptr.hpp>
#include <fc/variant.hpp>

#include <type_traits>

namespace fc {
using std::map;

/**
 *  @ingroup Serializable
 *
 *  @brief An order-perserving dictionary of variant's.
 *
 *  Keys are kept in the order they are inserted.
 *  This dictionary implements copy-on-write
 *
 *  @note This class is not optimized for random-access on large
 *        sets of key-value pairs.
 */
class variant_object {
private:
  /** @brief a key/value pair */
  class entry {
  public:
    entry();
    entry( const string& k, const variant& v );
    entry( entry&& e );
    entry( const entry& e );
    entry& operator=( const entry& );
    entry& operator=( entry&& );

    const string& key() const;
    const variant& value() const;
    void set( const variant& v );

    variant& value();

  private:
    string _key;
    variant _value;
  };

public:
  typedef std::vector< entry >::iterator iterator;
  typedef std::vector< entry >::const_iterator const_iterator;

  /**
   * @name Immutable Interface
   *
   * Calling these methods will not result in copies of the
   * underlying type.
   */
  ///@{
  iterator begin() const;
  iterator end() const;
  iterator find( const string& key ) const;
  iterator find( const char* key ) const;
  bool contains( const string& key ) const;
  bool contains( const char* key ) const;
  const variant& operator[]( const string& key ) const;
  const variant& operator[]( const char* key ) const;
  size_t size() const;
  ///@}
  variant& operator[]( const string& key );
  variant& operator[]( const char* key );

  /**
   * @name mutable Interface
   *
   * Calling these methods will result in a copy of the underlying type
   * being created if there is more than one reference to this object.
   */
  ///@{
  void reserve( size_t s );
  iterator begin();
  iterator end();
  void erase( const string& key );
  /**
   *
   * @return end() if key is not found
   */
  iterator find( const string& key );
  iterator find( const char* key );


  /** replaces the value at \a key with \a var or insert's \a key if not found */
  variant_object& set( const string& key, const variant& var );
  /** Appends \a key and \a var without checking for duplicates, designed to
   *  simplify construction of dictionaries using (key,val)(key2,val2) syntax
   */
  /**
   *  Convenience method to simplify the manual construction of
   *  variant_object's
   *
   *  Instead of:
   *    <code>variant_object("c",c).set("a",a).set("b",b);</code>
   *
   *  You can use:
   *    <code>variant_object( "c", c )( "b", b)( "c",c )</code>
   *
   *  @return *this;
   */
  variant_object& operator()( const string& key, const variant& var );
  template< typename T >
  variant_object& operator()( const string& key, T&& var )
  {
    set( key, variant( fc::forward< T >( var ) ) );
    return *this;
  }
  /**
   * Copy a variant_object into this variant_object.
   */
  variant_object& operator()( const variant_object& vo );

  template< typename T >
  explicit variant_object( T&& v ) : _key_value( new std::vector< entry >() )
  {
    *this = variant( fc::forward< T >( v ) ).get_object();
  }

  variant_object();

  template< typename T >
  variant_object( const map< string, T >& values ) : _key_value( new std::vector< entry >() )
  {
    _key_value->resize( values.size() );
    for( const auto& item: values )
    {
      _key_value->emplace_back( item.first, fc::variant( item.second ) );
    }
  }

  /** initializes the first key/value pair in the object */
  variant_object( const string& key, const variant& val );
  template< typename T >
  variant_object( const string& key, T&& val ) : _key_value( new std::vector< entry >() )
  {
    set( key, variant( forward< T >( val ) ) );
  }

  variant_object( variant_object&& );
  variant_object( const variant_object& );

  variant_object& operator=( variant_object&& obj );
  variant_object& operator=( const variant_object& obj );

private:
  std::shared_ptr< std::vector< entry > > _key_value;
};
/** @ingroup Serializable */
void to_variant( const variant_object& var, variant& vo );
/** @ingroup Serializable */
void from_variant( const variant& var, variant_object& vo );

template< typename T >
void to_variant( const std::map< string, T >& var, variant& vo )
{
  vo = variant_object( var );
}

template< typename T >
void from_variant( const variant& var, std::map< string, T >& vo )
{
  const auto& obj = var.get_object();
  vo.clear();
  for( auto itr = obj.begin(); itr != obj.end(); ++itr )
    vo[itr->key()] = itr->value().as< T >();
}

} // namespace fc
FC_REFLECT_TYPENAME( fc::variant_object )
