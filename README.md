# fc-variant-optimization-tests
`fc::variant` optimization tests

## Building
### Building on Ubuntu 18.04/20.04
For Ubuntu 18.04/20.04 users, after installing the right packages with apt, fc-variant-optimization-tests's
programs will build out of the box without further effort:
```bash
sudo apt update

# Install required compilation tools
sudo apt install -y \
    cmake \
    g++ \
    git \
    make

git clone https://gitlab.com/mtyszczak/fc-variant-optimization-tests.git
cd coze
git checkout master
mkdir build && cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make -j$(nproc) variant-optimization
# optional
make install  # defaults to /usr/local
```

## Profiling
This program supports valgrind detailed profiling:

First you have to configure your build to work in `Debug` or `RelWithDebInfo` (preferred),
which automatically adds profiling options to the compiler (`-pg`):
```bash
pwd # First make sure that you are in the build directory
# If you are not then cd into it
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ..
make -j$(nproc) variant-optimization
```

Then install required profiling tools and profile:
```bash
# Install required profiling tools
sudo apt install -y valgrind \
                 kcachegrind # Optional, but provides GUI for the profiler results

valgrind --tool=callgrind --instr-atstart=no --collect-jumps=yes \
         --dump-instr=yes --collect-systime=usec --callgrind-out-file=callgrind.out \
         ./programs/variant-optimization/variant-optimization ../data/blocks.json
```

And now you are able to inspect on the results:
```bash
kcachegrind ./callgrind.out
```

## License
See [LICENSE.md](LICENSE.md)
