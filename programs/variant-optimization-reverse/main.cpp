#include <fstream>
#include <string>
#include <iostream>
#include <cstdlib>

#include <fc/exception/exception.hpp>
#include <fc/log/logger.hpp>
#include <fc/io/json.hpp>
#include <fc/variant.hpp>

#include <hive/protocol/block.hpp>

#include <callgrind.h>

int main( int argc, char** argv )
{
  try
  {
    FC_ASSERT( argc > 1 && argc < 4, "Invalid number of arguments: ${argc}. "
      "Usage: \'variant-optimization <blocks_data_json> <optional_num_of_blocks>''", ("argc",argc) );

    std::ifstream file( argv[1] );
    FC_ASSERT( file.is_open(), "Could not open the file: ${file}", ("file",argv[1]) );

    std::string content((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());

    fc::variants v = fc::json::from_string( content )["result"]["blocks"].get_array();

    fc::variants v_out;
    v_out.resize( v.size() );

    size_t num_of_blocks = argc == 3 ? std::atoi( argv[2] ) : v.size();

    FC_ASSERT(num_of_blocks >= 0 && num_of_blocks <= v.size(), "Invalid number of blocks: ${num_of_blocks}. Use value in range: 0-${v_size}",
      ("num_of_blocks",num_of_blocks)("v_size",v.size()) );

    for( size_t i = 0; i < num_of_blocks; ++i )
    {
      std::cout << std::setw(3) << unsigned(float(i)/num_of_blocks * 100) << "% Done\r" << std::flush;

      hive::protocol::signed_block sb;
      fc::from_variant( v.at(i), sb );
      FC_ASSERT( sb.id() != hive::protocol::block_id_type{}, "Invalid data inside of the block" );

      CALLGRIND_START_INSTRUMENTATION;
      CALLGRIND_TOGGLE_COLLECT;
      fc::to_variant( sb, v_out.at(i) );
      CALLGRIND_TOGGLE_COLLECT;
      CALLGRIND_STOP_INSTRUMENTATION;
    }

    std::cout << std::endl;

    file.close();
    return 0;
  } FC_CAPTURE_AND_LOG( (argc) )

  return -1;
}
