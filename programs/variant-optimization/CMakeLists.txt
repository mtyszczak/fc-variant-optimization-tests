add_executable( variant-optimization main.cpp )

find_path( VALGRIND_CALLGRIND_PATH "valgrind/callgrind.h" REQUIRED )
set( VALGRIND_INCLUDE_PATH "${VALGRIND_CALLGRIND_PATH}/valgrind" )

message( STATUS "Valgrind Callgrind found with include path: '${VALGRIND_INCLUDE_PATH}'" )

target_include_directories( variant-optimization PRIVATE "${VALGRIND_INCLUDE_PATH}" )

if( HIVE_STATIC_BUILD )
  target_link_libraries( variant-optimization PRIVATE
                        "-static-libstdc++ -static-libgcc"
                        fc hive_protocol
  )
else()
  target_link_libraries( variant-optimization PRIVATE
                        fc hive_protocol
  )
endif()

install( TARGETS
variant-optimization

  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
)
